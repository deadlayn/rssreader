//
//  DateExtension.swift
//  RSSR
//
//  Created by admin on 17.04.2021.
//

import Foundation

extension Date {
    static func from(_ value: String, format: String, local: Locale) -> Date? {
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "EN")
        dateFormatter.dateFormat = format
        if let newDate = dateFormatter.date(from: value) {
            return newDate
        } else { return nil }
    }
    
    func to(format: String) -> String {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate(format)
        let dateResult = formatter.string(from: self)
        return dateResult
    }

    func get(_ component: Calendar.Component) -> Int { //swiftlint:disable:this type_name
        let requestedComponents: Set<Calendar.Component> = [
            .year, .month, .day, .hour, .minute, .second
        ]

        let userCalendar = Calendar.current
        var dateTimeComponents: DateComponents {
            return userCalendar.dateComponents(requestedComponents, from: self)
        }

        switch component {
        case .year: return dateTimeComponents.year ?? 0
        case .month: return dateTimeComponents.month ?? 0
        case .day: return dateTimeComponents.day ?? 0
        case .hour: return dateTimeComponents.hour ?? 0
        case .minute: return dateTimeComponents.minute ?? 0
        case .second: return dateTimeComponents.second ?? 0
        default: return 0
        }
    }

    static func getMonth(num: Int) -> String {
        let dateFormatter = DateFormatter()
        return dateFormatter.monthSymbols[num - 1]
    }
}
