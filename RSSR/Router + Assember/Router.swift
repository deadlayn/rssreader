//
//  Router.swift
//  RSSR
//
//  Created by admin on 17.04.2021.
//

import UIKit

protocol RouterProtocol {
    var navigationController: UINavigationController? { get set }
    var assemblerModule: AssemblerModuleProtocol? { get set }
    
    func listNewsViewController(channelInfo: ChannelInfo)
    func listChannelsViewController()
    func readerNewsViewController(newsData: NewsData)
    func popToRoot()
}

class Router: RouterProtocol {
    public var navigationController: UINavigationController?
    public var assemblerModule: AssemblerModuleProtocol?

    init(navigationController: UINavigationController, assemblerModule: AssemblerModuleProtocol) {
        self.navigationController = navigationController
        self.assemblerModule = assemblerModule
    }

    func listChannelsViewController() {
        if let navController = navigationController {
            guard let viewController = assemblerModule?.createListChannelsModule(router: self) else { return }
            navController.viewControllers = [viewController]
        }
    }

    func listNewsViewController(channelInfo: ChannelInfo) {
        if let navController = navigationController {
            guard let viewController = assemblerModule?.createListNewsModule(router: self,
                    channelInfo: channelInfo) else { return }
            navController.pushViewController(viewController, animated: true)
        }
    }

    func readerNewsViewController(newsData: NewsData) {
        if let navController = navigationController {
            guard let viewController = assemblerModule?.createReaderNewsModule(router: self, newsData: newsData) else { return }
            navController.pushViewController(viewController, animated: true)
        }
    }

    func popToRoot() {
        if let navController = navigationController {
            navController.popToRootViewController(animated: true)
        }
    }
}
