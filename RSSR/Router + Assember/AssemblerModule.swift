//
//  AssemblerModule.swift
//  RSSR
//
//  Created by admin on 17.04.2021.
//

import UIKit

protocol AssemblerModuleProtocol {
    func createListNewsModule(router: RouterProtocol, channelInfo: ChannelInfo) -> UIViewController
    func createListChannelsModule(router: RouterProtocol) -> UIViewController
    func createReaderNewsModule(router: RouterProtocol, newsData: NewsData) -> UIViewController
}

class AssemblerModule: AssemblerModuleProtocol {
    func createListChannelsModule(router: RouterProtocol) -> UIViewController {
        let viewController = ListChannelsViewController()

        let presenter = ListChannelsPresenter(view: viewController, router: router)
        viewController.presenter = presenter
        return viewController
    }

    func createListNewsModule(router: RouterProtocol, channelInfo: ChannelInfo) -> UIViewController {
        var viewController: ListNewsViewController

        let identifier = String(describing: ListNewsViewController.self)
        viewController = createVC(storyboardName: "ListNews", identifier: identifier)

        let presenter = ListNewsPresenter(view: viewController, router: router, channelInfo: channelInfo)
        viewController.presenter = presenter
        return viewController
    }

    func createReaderNewsModule(router: RouterProtocol, newsData: NewsData) -> UIViewController {
        var viewController: ReaderNewsViewController

        let identifier = String(describing: ReaderNewsViewController.self)
        viewController = createVC(storyboardName: "ReaderNews", identifier: identifier)

        let presenter = ReaderNewsPresenter(view: viewController, router: router, newsData: newsData)
        viewController.presenter = presenter
        return viewController
    }
}

private extension AssemblerModule {
    
    func createVC<T: UIViewController>(storyboardName: String, identifier: String) -> T {
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: .none)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
            fatalError("Failed to load \(identifier) from storyboard.")
        }
        return viewController
    }
}
