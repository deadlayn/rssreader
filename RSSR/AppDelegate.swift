//
//  AppDelegate.swift
//  RSSR
//
//  Created by admin on 17.04.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        initialViewController()
        return true
    }

    private func initialViewController() {
        window = UIWindow(frame: UIScreen.main.bounds)

        let assemblerModule = AssemblerModule()
        let navigationController = UINavigationController()
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()

        let router = Router(navigationController: navigationController,
                            assemblerModule: assemblerModule)
        router.listChannelsViewController()
    }
}

