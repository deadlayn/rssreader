//
//  NewsFeedLoader.swift
//  RSSR
//
//  Created by admin on 17.04.2021.
//

import Foundation
import FeedKit

protocol NewsFeedLoaderProtocol {
    func fetch(feed: URL?, completion: @escaping (Result<[NewsData], NewsLoadingError>) -> Void)
}

class NewsFeedLoader: NewsFeedLoaderProtocol {
    public func fetch(feed: URL?, completion: @escaping (Result<[NewsData], NewsLoadingError>) -> Void) {

        guard let feed = feed else { return }
        let request = URLRequest(url: feed, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 60)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                DispatchQueue.main.async {
                    completion(.failure(.networkingError(error)))
                }
                return
            }

            guard let http = response as? HTTPURLResponse else { return }
            switch http.statusCode {
            case 200:
                if let data = data {
                    self.loadFeed(data: data, completion: completion)
                }
            case 404:
                DispatchQueue.main.async {
                    completion(.failure(.notFound))
                }
            case 500...599:
                DispatchQueue.main.async {
                    completion(.failure(.serverError(http.statusCode)))
                }
            default:
                DispatchQueue.main.async {
                    completion(.failure(.requestFailed(http.statusCode)))
                }
            }
        }.resume()
    }
    
    private func loadFeed(data: Data, completion: @escaping (Result<[NewsData], NewsLoadingError>) -> Void) {
        let parser = FeedParser(data: data)
        parser.parseAsync { parseResult in
            let result: Result<[NewsData], NewsLoadingError>
            do {
                switch parseResult {
                case .success(let feed):
                    switch feed {
                    case .atom(_): fatalError()
                    case .rss(let rss): result = try .success(self.convert(rss: rss))
                    case .json(_): fatalError()
                    }
                case .failure(let error):
                    result = .failure(.feedParsingError(error))
                }
            } catch let error as NewsLoadingError {
                result = .failure(error)
            } catch {
                fatalError()
            }
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    private func convert(rss: RSSFeed) throws -> [NewsData] {
        var newsData = [NewsData]()
        guard let items = rss.items else { return newsData }
        for item in items {
            
            guard let title = item.title else {
                throw NewsLoadingError.missingAttribute("title")
            }
            
            guard let date = item.pubDate else {
                throw NewsLoadingError.missingAttribute("date")
            }
            
            guard let description = item.description else {
                throw NewsLoadingError.missingAttribute("description")
            }

            guard let link = item.link else {
                throw NewsLoadingError.missingAttribute("link")
            }
            
            newsData.append(NewsData(title: title,
                                     description: description,
                                     date: date, link: link))
        }
        return newsData
    }
}
