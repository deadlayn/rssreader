//
//  NewsLoadingError.swift
//  RSSR
//
//  Created by admin on 17.04.2021.
//

import Foundation

enum NewsLoadingError: Error {
    case networkingError(Error)
    case requestFailed(Int)
    case serverError(Int)
    case notFound
    case feedParsingError(Error)
    case missingAttribute(String)

    var localized: String {
        switch self {
        case .networkingError(_):
            return "Failed to connect"
        case .requestFailed(_):
            return "Request failed"
        case .serverError(_):
            return "Server Error"
        case .notFound:
            return "The server cannot find the requested resource."
        case .feedParsingError(_):
            return "The program cannot read this format"
        case .missingAttribute(_):
            return "Attribute error"
        }
    }
}
