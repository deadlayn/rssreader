//
//  ViewController.swift
//  RSSR
//
//  Created by admin on 17.04.2021.
//

import UIKit

class ListNewsViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var switchDescriptionButton: UIBarButtonItem!
    
    // MARK: - Variables
    public var presenter: ListNewsPresenter!
    private var dataSource: ListNewsTableViewDataSource<NewsTableViewCell, NewsData>!
    private var indicator: UIActivityIndicatorView!
    private var modeDescription: ModeDescription = .mini {
        didSet {
            switch modeDescription {
            case .full:
                switchDescriptionButton.title = "FULL"
            case .mini:
                switchDescriptionButton.title = "MINI"
            }
        }
    }

    // MARK: - Cycle life
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: - Setup
    private func setup() {
        createIndicator()
        setupTableView()
        setupSwitchDescriptionButton()
    }

    private func setupTableView() {
        tableView.delegate = self
        tableView.isHidden = true
    }

    private func setupSwitchDescriptionButton() {
        switchDescriptionButton.target = self
        switchDescriptionButton.action = #selector(switchDescriptionButtonAction)
    }

    private func createIndicator() {
        indicator = UIActivityIndicatorView()
        indicator.center = tableView.center
        indicator.startAnimating()
        view.addSubview(indicator)
    }

    // MARK: - Actions
    @objc
    private func switchDescriptionButtonAction() {
        modeDescription = modeDescription == .mini ? .full : .mini

        UIView.animate(withDuration: 0.2, animations: {
            self.tableView.alpha = 0
        }, completion: { _ in
            self.tableView.reloadData()
            UIView.animate(withDuration: 0.2) {
                self.tableView.alpha = 1
            }
        })
    }

    private func openBrowser(link: String) {
        let urlNews = URL(string: link)
        if let urlNews = urlNews {
            UIApplication.shared.open(urlNews, options: [:], completionHandler: nil)
        } else {
            createAlert(message: "Link to news was not found")
        }
    }

    private func updateDataSource(sections: [String], items: [[NewsData]]) {
        let identifier = String(describing: NewsTableViewCell.self)
        dataSource = ListNewsTableViewDataSource(cellIdentifier: identifier, sections: sections,
                items: items, configureCell: { [weak self] (cell, news) in
            cell.news = news
            cell.modeDescription = self?.modeDescription
        })

        DispatchQueue.main.async {
            self.tableView.dataSource = self.dataSource
            self.tableView.reloadData()
            self.tableView.isHidden = false
            self.indicator.stopAnimating()
        }
    }
}

extension ListNewsViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? NewsTableViewCell {
            if let newsData = cell.news {
                switch modeDescription {
                case .full:
                    openBrowser(link: newsData.link)
                case .mini:
                    presenter.routeToReaderNews(newsData: newsData)
                }
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ListNewsViewController: ListNewsViewProtocol {
    func createAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action {
            default: self.presenter.routeToListChannels()
            }
        }))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }

    func setupTitle(_ title: String) {
        self.title = title
    }

    func successLoadNews(sections: [String], items: [[NewsData]]) {
        updateDataSource(sections: sections, items: items)
    }
}

