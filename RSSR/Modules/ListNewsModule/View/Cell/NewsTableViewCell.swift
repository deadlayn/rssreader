//
//  NewsTableViewCell.swift
//  RSSR
//
//  Created by admin on 17.04.2021.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    // MARK: - Variables

    public var modeDescription: ModeDescription? = .mini {
        didSet {
            switch modeDescription {
            case .full:
                descriptionLabel.numberOfLines = 0
            case .mini, .none:
                descriptionLabel.numberOfLines = 3
            }
        }
    }

    public var news: NewsData? {
        didSet {
            nameLabel.text = news?.title
            dateLabel.text = news?.date.to(format: "yyyy.MM.dd HH:mm")
            descriptionLabel.text = normalizeText(news?.description)
        }
    }
    
    // MARK: - Actions
    private func normalizeText(_ text: String?) -> String {
        guard let text = text else { return String() }
        return text.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
