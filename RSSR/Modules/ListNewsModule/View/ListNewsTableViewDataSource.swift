//
//  TableViewDataSource.swift
//  RSSR
//
//  Created by admin on 17.04.2021.
//

import UIKit

class ListNewsTableViewDataSource<CELL : UITableViewCell, T> : NSObject, UITableViewDataSource {
    
    private var cellIdentifier : String!
    private var sections : [String]!
    private var items : [[T]]!
    public var configureCell : (CELL, T) -> Void = {_, _ in }
    
    init(cellIdentifier : String, sections: [String], items: [[T]], configureCell: @escaping (CELL, T) -> Void) {
        self.cellIdentifier = cellIdentifier
        self.sections = sections
        self.items = items
        self.configureCell = configureCell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items[section].count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        sections[section]
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CELL {
            let item = self.items[indexPath.section][indexPath.row]
            self.configureCell(cell, item)
            return cell
        }
        
        return UITableViewCell()
    }
}
