//
//  NewsData.swift
//  RSSR
//
//  Created by admin on 17.04.2021.
//

import Foundation

struct NewsData {
    let title: String
    let description: String
    let date: Date
    let link: String
}
