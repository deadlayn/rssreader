//
//  MainPresenter.swift
//  RSSR
//
//  Created by admin on 17.04.2021.
//

import Foundation

protocol ListNewsViewProtocol {
    func successLoadNews(sections: [String], items: [[NewsData]])
    func setupTitle(_ title: String)
    func createAlert(message: String)
}

protocol ListNewsPresenterProtocol {
    init(view: ListNewsViewProtocol, router: RouterProtocol, channelInfo: ChannelInfo)
    func routeToListChannels()
}

class ListNewsPresenter: ListNewsPresenterProtocol {
    private var view: ListNewsViewProtocol!
    private var router: RouterProtocol!
    private var channelInfo: ChannelInfo!
    
    required init(view: ListNewsViewProtocol, router: RouterProtocol, channelInfo: ChannelInfo) {
        self.view = view
        self.router = router
        self.channelInfo = channelInfo

        setupTitle()
        loadNewsData()
    }

    public func routeToListChannels() {
        router.popToRoot()
    }

    public func routeToReaderNews(newsData: NewsData) {
        router.readerNewsViewController(newsData: newsData)
    }

    private func setupTitle() {
        view.setupTitle(channelInfo.name)
    }
    
    private func divMonths(newsData: [NewsData]) -> [Int: [NewsData]] {
        var newsDataMonths = [Int: [NewsData]]()

        for news in newsData {
            if newsDataMonths[news.date.get(.month)] == nil {
                newsDataMonths[news.date.get(.month)] = [NewsData]()
            }
            newsDataMonths[news.date.get(.month)]?.append(news)
        }

        return newsDataMonths
    }

    private func sortedNewsDataMonths(_ newsDataMonths: [Int: [NewsData]]) -> (months: [String], newsData: [[NewsData]]) {
        var result = (months: [String](), newsData: [[NewsData]]())
        let months = newsDataMonths.keys.sorted(by: {$0 < $1})

        for month in months {
            let monthString = Date.getMonth(num: month)
            result.months.append(monthString)
            if let newsData = newsDataMonths[month] {
                result.newsData.append(newsData.sorted(by: {$0.date < $1.date}))
            }
        }

        return result
    }

    private func loadNewsData() {
        let newsFeedLoader: NewsFeedLoaderProtocol = NewsFeedLoader()
        newsFeedLoader.fetch(feed: channelInfo.url) { result in
            switch result {
            case .success(let newsData):
                let newsDataMonths = self.divMonths(newsData: newsData)
                let sortedNewsDataMonths = self.sortedNewsDataMonths(newsDataMonths)
                self.view.successLoadNews(sections: sortedNewsDataMonths.months, items: sortedNewsDataMonths.newsData)

            case .failure(let error):
                self.view.createAlert(message: error.localized)
            }
        }
    }
}
