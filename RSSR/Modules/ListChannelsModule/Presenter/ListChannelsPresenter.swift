//
// Created by admin on 18.04.2021.
//

import Foundation

protocol ListChannelsViewProtocol: class {
    func loadChannelsList(data: [ChannelInfo])
}

protocol ListChannelsPresenterProtocol {
    init(view: ListChannelsViewProtocol, router: RouterProtocol)
    func routeListNews(index: Int)
}

class ListChannelsPresenter: ListChannelsPresenterProtocol {
    private weak var view: ListChannelsViewProtocol?
    private var router: RouterProtocol!

    private let listChannels: [ChannelInfo] = [
        ChannelInfo(name: "Lenta.ru : Новости", url: URL(string: "https://lenta.ru/rss/")),
        ChannelInfo(name: "NASA Breaking News", url: URL(string: "https://www.nasa.gov/rss/dyn/breaking_news.rss")),
        ChannelInfo(name: "iTunes Store : Top Films", url: URL(string: "https://rss.itunes.apple.com/api/v1/us/movies/top-movies/all/10/explicit.rss")),
        ChannelInfo(name: "Нерабочая ссылка", url: URL(string: "https://lenta.ru/rsss/")),
    ]

    required init(view: ListChannelsViewProtocol, router: RouterProtocol) {
        self.view = view
        self.router = router

        loadChannelsList()
    }

    private func loadChannelsList() {
        view?.loadChannelsList(data: listChannels)
    }

    public func routeListNews(index: Int) {
        router.listNewsViewController(channelInfo: listChannels[index])
    }
}