//
// Created by admin on 18.04.2021.
//

import UIKit

class ListChannelsViewController: UIViewController {

    // MARK: - Variables
    public var presenter: ListChannelsPresenterProtocol!
    private var tableView: UITableView!
    private var dataSource: ListChannelsTableViewDataSource<ChannelInfoTableViewCell, ChannelInfo>!

    // MARK: - Cycle life
    override func viewDidLoad() {
        super.viewDidLoad()

        create()
        setup()
    }

    // MARK: - Setup
    private func setup() {
        view.backgroundColor = .white
        title = "Channels"

        setupTableView()
    }

    private func setupTableView() {
        tableView.delegate = self

        let identifier = String(describing: ChannelInfoTableViewCell.self)
        tableView.register(ChannelInfoTableViewCell.self, forCellReuseIdentifier: identifier)
    }

    // MARK: - Create
    private func create() {
        createTableView()
    }

    private func createTableView() {
        tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
    }

    // MARK: - Actions
    private func updateDataSource(data: [ChannelInfo]) {
        let identifier = String(describing: ChannelInfoTableViewCell.self)
        dataSource = ListChannelsTableViewDataSource(cellIdentifier: identifier, items: data, configureCell: { (cell, rss) in
            cell.textLabel?.text = rss.name
        })

        DispatchQueue.main.async {
            self.tableView.dataSource = self.dataSource
            self.tableView.reloadData()
        }
    }
}

extension ListChannelsViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.routeListNews(index: indexPath.item)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ListChannelsViewController: ListChannelsViewProtocol {
    func loadChannelsList(data: [ChannelInfo]) {
        updateDataSource(data: data)
    }
}
