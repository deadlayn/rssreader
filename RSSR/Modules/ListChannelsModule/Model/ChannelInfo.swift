//
// Created by admin on 18.04.2021.
//

import Foundation

struct ChannelInfo {
    let name: String
    let url: URL?
}