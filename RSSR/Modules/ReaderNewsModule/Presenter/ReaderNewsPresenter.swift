//
// Created by admin on 18.04.2021.
//

import Foundation

protocol  ReaderNewsViewProtocol {
    func setupNewsData(_ newsData: NewsData)
}

protocol ReaderNewsPresenterProtocol {
    init(view: ReaderNewsViewProtocol, router: RouterProtocol, newsData: NewsData)
}

class ReaderNewsPresenter: ReaderNewsPresenterProtocol {
    private var view: ReaderNewsViewProtocol!
    private var router: RouterProtocol!
    private var newsData: NewsData!

    required init(view: ReaderNewsViewProtocol, router: RouterProtocol, newsData: NewsData) {
        self.view = view
        self.router = router
        self.newsData = newsData

        setupNewsData()
    }

    func setupNewsData() {
        DispatchQueue.main.async {
            self.view.setupNewsData(self.newsData)
        }
    }
}