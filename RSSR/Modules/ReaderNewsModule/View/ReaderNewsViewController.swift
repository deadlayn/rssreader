//
//  ReaderNewsViewController.swift
//  RSSR
//
//  Created by admin on 18.04.2021.
//

import UIKit

class ReaderNewsViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var openNewsButton: UIBarButtonItem!
    
    // MARK: - Variables
    public var presenter: ReaderNewsPresenterProtocol!
    private var linkNews: String?

    // MARK: - Cycle life
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: - Setup
    private func setup() {
        setupOpenNewsButton()
    }

    private func setupOpenNewsButton() {
        openNewsButton.target = self
        openNewsButton.action = #selector(openNewsButtonAction)
    }

    // MARK: - Actions
    @objc
    private func openNewsButtonAction() {
        let urlNews = URL(string: linkNews ?? "")
        if let urlNews = urlNews {
            UIApplication.shared.open(urlNews, options: [:], completionHandler: nil)
        } else {
            createAlert(message: "Link to news was not found")
        }
    }

    func createAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: .none))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }

    private func normalizeText(_ text: String) -> String {
        return text.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}

extension ReaderNewsViewController: ReaderNewsViewProtocol {
    func setupNewsData(_ newsData: NewsData) {
        titleLabel.text = newsData.title
        dateLabel.text = newsData.date.to(format: "yyyy.MM.dd HH:mm")
        descriptionLabel.text = normalizeText(newsData.description)
        linkNews = newsData.link
    }
}
